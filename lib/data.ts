import React from "react";
import { CgWorkAlt } from "react-icons/cg";
import { FaReact } from "react-icons/fa";
import { LuGraduationCap } from "react-icons/lu";
import corpcommentImg from "@/public/chatApp.png";
import rmtdevImg from "@/public/quizApp.png";
import wordanalyticsImg from "@/public/TypescriptPage.png";

export const links = [
  {
    name: "Home",
    hash: "#home",
  },
  {
    name: "About",
    hash: "#about",
  },
  {
    name: "Projects",
    hash: "#projects",
  },
  {
    name: "Skills",
    hash: "#skills",
  },
  {
    name: "Experience",
    hash: "#experience",
  },
  {
    name: "Contact",
    hash: "#contact",
  },
] as const;

export const experiencesData = [
  {
    title: "Graduated IMS NOIDA",
    location: "NOIDA Uttar Pradesh",
    description:
      "I have completed my Bachelor of Computer Applications (BCA) from IMS Noida, where I honed my skills in various aspects of computer science and technology, laying a strong foundation for my journey into the world of programming and cybersecurity.",
    icon: React.createElement(LuGraduationCap),
    date: "2020",
  },
  {
    title: "web Developer",
    location: "Awesome Sauce Pvt Ltd",
    description:
      "Acquired expertise in HTML, CSS, JavaScript, jQuery, and PHP, applying this skill set to successfully deliver projects for the company’s clients",
    icon: React.createElement(CgWorkAlt),
    date: "2020 - 2021",
  },
  {
    title: "Frontend Developer",
    location: "Kratikal Tech Pvt Ltd.",
    description:
      "Developed SEO-friendly projects like Threatcop and Kratikal.com, achieving Core Web Vitals compliance. This resulted in a 90% increase in user retention and an 80% boost in global leads. Implemented Next.js to enhance search engine optimization (SEO) performance and successfully addressed various issues in the TSAT tool.",
    icon: React.createElement(FaReact),
    date: "2022 - present",
  },
] as const;

export const projectsData = [
  {
    title: "Chat Application",
    description:
      "I worked as a full-stack developer on this project. Users can chat with each other",
    tags: ["React", "MongoDB", "Nodejs", "Express"],
    imageUrl: corpcommentImg,
  },
  {
    title: "React Quiz App",
    description:
      "This app is created for playing quiz related to cybersecurity.",
    tags: ["Reactjs", "JSON data", "Material UI"],
    imageUrl: rmtdevImg,
  },
  {
    title: "Vanilla CSS Website",
    description:
      "This is simple landing page in which i have used pure CSS without using any framework also I have implemented JEST in this for UI component testing",
    tags: ["Nextjs", "Typescript", "CSS", "Jest"],
    imageUrl: wordanalyticsImg,
  },
] as const;

export const skillsData = [
  "HTML",
  "CSS",
  "JavaScript",
  "TypeScript",
  "React",
  "Next.js",
  "Node.js",
  "Git",
  "Tailwind",
  "MongoDB",
  "Redux",
  "Express",
] as const;
