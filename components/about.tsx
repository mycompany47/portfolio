"use client";

import React from "react";
import SectionHeading from "./section-heading";
import { motion } from "framer-motion";
import { useSectionInView } from "@/lib/hooks";

export default function About() {
  const { ref } = useSectionInView("About");

  return (
    <motion.section
      ref={ref}
      className="mb-28 max-w-[45rem] text-center leading-8 sm:mb-40 scroll-mt-28"
      initial={{ opacity: 0, y: 100 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.175 }}
      id="about"
    >
      <SectionHeading>About me</SectionHeading>
      <p className="mb-3">
        After graduating with a degree in {" "}
        <span className="font-medium">Computer Application</span>, I decided to pursue my passion for programming{" "}
        <span className="font-medium">I started as an intern and learned full-stack web development. My core stack is React, Next.js, Node.js, and MongoDB. I am also familiar with TypeScript. I am always looking to learn new technologies. I am currently looking for a full-time position as a software developer.</span>.{" "}
        <span className="italic">When I'm not coding, I enjoy playing video games, watching movies, and playing with my dog.</span>
      </p>
    </motion.section>
  );
}
